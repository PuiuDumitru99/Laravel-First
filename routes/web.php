<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Posts\CommentController;

use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Middleware\logMiddleware;
use App\Http\Controllers\BusketController;

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TestController;
use App\Http\Middleware\tokenMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Testing\TestComponent;

Route::name('user.')->group(function(){
    Route::get('/user',[UserController::class,'index'])->name('user.index');
    Route::get('/user/show/{user}',[UserController::class,'show'])->middleware('auth')->name('show');

route::get('/login',function(){

    if(Auth::check())
    {
        return redirect(route('user.show'));
    }
    return view('login.index');
})->name('login');



});



Route::get('/', 'HomeController@index');
Route::post('/home/{produts}', [HomeController::class,'show'])->name('home.show');
//Route::get('/user/', [UserController::class,'index'])->name('user.index');
//Route::get('/user/show/{user}',  [UserController::class,'show'])->name('user.show');

Route::get('/busket', 'BusketController@index')->name('busket');
Route::post('busket/{busket}', [BusketController::class,'store'])->name('busket.store');
Route::get('busket/{busket}', [BusketController::class,'show'])->name('busket.show');


Route::get('register',[RegisterController::class,'index'])->name('register.index');
Route::post('register/store',[RegisterController::class,'store'])->name('register.store');
//Route::get('login', [LoginController::class,'index'])->name('login.index');
Route::get('login/search', [LoginController::class,'search'])->name('login.search');

Route::get('post','PostController@index');
Route::get('post/search', [PostController::class,'search'])->name('post.search');
Route::post('post/store', [PostController::class,'store'])->name('post.store');
Route::get('post/create', [PostController::class,'create'])->name('post.create');
Route::get('post/show/{post}', [postController::class,'show'])->name('post.show');


Route::get('/about', 'AboutController@index');


Route::get('contact', 'ContactController@index')->name('Contact');


Route::get('test',[TestController::class,'index'])->name('test');


