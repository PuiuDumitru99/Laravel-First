@push ('css')
<link rel="stylesheet" href="/css/owl.carousel.min.css">
@endpush

<div class="owl-carousel">  

{{$slot}}

</div>
@push('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="/js/owl.carousel.js"></script>
<script>
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>

@endpush