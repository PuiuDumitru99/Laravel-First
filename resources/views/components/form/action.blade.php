@props(['method'=>'GET'])
@php
    $method=strtoupper($method)
@endphp
<form {{$attributes}}  method="{{$method}}" class="mb-2 d-flex justify-content-around" >
    @if ($method !=='GET')
        @csrf   
    @endif
    {{$slot}}
</form>
