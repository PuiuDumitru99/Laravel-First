@php
    $id=Str::uuid()
@endphp
  <input class="form-check-input" {{$attributes}} type="checkbox" id="{{$id}}">
  <label class="form-check-label" for="{{$id}}">
    {{$slot}}
  </label>


