
@props(['method'=>'GET'])
@php
    $method=strtoupper($method)
@endphp
<form {{$attributes}}  method="{{$method}}" class="card-body d-grid mb-3" >
    @if ($method !=='GET')
        @csrf   
    @endif
    {{$slot}}
</form>
