<button  {{$attributes->merge(['type'=>'submit'])}} class="btn m-1 btn-primary align-items-center">
    {{$slot}}
</button>
