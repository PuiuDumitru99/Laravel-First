<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.1/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="/css/style.css">
    
    @stack ('css')

    <title>@yield('page.title',config('app.name'))</title>
</head>
<body>
    <x-container>
        @include('includes.alert')
        @include('includes.wrong')
        @include('includes.header')
    </x-container>
    <div class="min-vh-100 mt-1">
            @yield('content')
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    @stack('js')

    @include('includes.footer') 
</body>

</html>