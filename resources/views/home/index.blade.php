@extends('layouts.base')

@section('page.title')Home page @endsection

@push('css')
<link rel="stylesheet" href="/css/home.css">    
@endpush

@section('content')
<x-container>
    <div class="row align-items-center d-flex justify-content-end">
        <div class="col-md-4 col-sm-5 text_bilbord">
            <p>Simple Furniture with<a href=""> high-end </a>quality
            </p>
            <p for="">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </p>
            <a href="post">
                <button class="btn-brown">Shop now</button>
            </a>                
        </div>
        <div class="col-md-8 col-sm-7 ">
            <img src="/img/pic.png" alt="">
        </div>
    </div>
    <div class="row justify-content-center mb-5">
        <div class="col-12">
            <div class="about">
                <div class="poster">
                </div>
                <div class="text">
                    <a href="">About Us</a>
                    <span>
                        FURNITURE OF ART
                    </span>
                    <span>
                        Lorem Ipsum is simply dummy text of the printing anLorem Ipsum is simply dummy 
                        text of the printing and typesetting industry.d typesetting industry.
                    </span>
                    <a href="post">
                        <button class="btn-brown">Shop now</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-center  mt-3 mb-3" >
        <div class="text1">
            <strong>WHAT PRODUCT WE SELL</strong>
            <p>Find our best products faster, better and qualitier product’s choices from us.</p>
        </div>
        <x-carousel>
            @foreach ($products as $product )  
            <div class="item">
                <div class="post">
                    <x-form.title><a href="{{route('post.show', $product->id)}}">{{$product->title}}</a></x-form.title>
                    <x-form.body>
                        <img src="../img/furniture/{!!$product->img!!}" alt="">  
                      </x-form.body> 
                    <x-form.item>{{$product->price}}</x-form.item>                      
                    <x-form.action action="{{route('busket.store',$product->id)}}" method="post" class="form-body">
                        <x-butt-by>BUY</x-butt-by>
                    </x-form.action>
                </div>
            </div>
            @endforeach
        </x-carousel>   
    </div>
    <div class="row justify-content-center mt-3">
        <div class="col special-offert">
            <span>
                SPECIAL DEALS FOR THIS WEEK
            </span>
            <div class="timer">
                <div>
                    <p id="days"></p>
                    <i>Days</i>
                </div>
                <div>
                    <p id="hours"></p>
                    <i >Hours</i>
                </div>
                <div>
                    <p id="min"></p>
                    <i >Minutes</i>
                </div>
                <div>
                    <p id="sec"></p>
                    <i >Second</i>
                </div>
            </div>
            <a href="post">
                <button class="btn-brown">Shop now</button>
            </a> 
        </div>

    </div>
</x-container>
@endsection
@push('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="../js/time.js"></script>
@endpush