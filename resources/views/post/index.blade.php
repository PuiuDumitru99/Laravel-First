
@extends('layouts.base')

@section('page.title')List of Products @endsection

@section('content')
<x-container class="overflow-hidden">
    <div class="row justify-content-center pt-1">
        <div class="col-4 col-sm-4 col">
            <x-card.wrapper class="m-3">
                <x-card.body action="{{route('post.search')}}">
                    <x-card.title> Search by name </x-card.title>
                    <x-input name="search"  placeholder="Write title the product"/>
                    <br>
                    <x-card.action>
                        <x-button>{{__('Search')}}</x-button>
                        <a href="/post" class="btn m-1 btn-primary"><-</a>
                        <a href="/post/create" class="btn m-1 btn-primary"> {{__('Create')}}</a>
                    </x-card.action>
                </x-card.body>
            </x-card.wrapper>
        </div>
        <div class="col-12 d-flex flex-wrap justify-content-center">            
            @if (empty($products))
            {{ __('Nothing post`s page')}}
            @else
            @foreach ($products as $product )  
                <x-form.wrapper>
                    <x-form.title>
                        <a href="{{route('post.show', $product->id)}}">{{$product->title}}</a>
                    </x-form.title>
                    <x-form.body>
                        <img src="../img/furniture/{!!$product->img!!}" alt="">
                      </x-form.body>                     
                        <x-form.item>
                            {!!$product->price!!}
                            </x-form.item>   
                    <x-form.action action="{{route('busket.store',$product->id)}}" method="post">
                        <x-butt-by>
                         BUY
                        </x-butt-by>
                    </x-form.action>
                </x-form.wrapper> 
            @endforeach
        
            {{$products->links()}}
            @endif
        </div>
    </div>
</x-container>
@endsection
