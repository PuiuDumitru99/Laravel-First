@extends('layouts.base')

@section('content')
<x-container>
   <div class="row justify-content-center">
        <div class="col col-8 col-lg-4 col-sm-8 col-md-6 col-s-10">
            <x-card.wrapper>
                <x-card.body action="{{route('post.store')}}" method="post" >
                        <x-label for="">{{__('Title post')}}</x-label>
                        <x-input name="title_post"/>
                        <x-label for="">{{__('Text area')}}</x-label>
                        <x-text-aria name="data_post"></x-text-aria>
                        <x-label for="">{{__('Insert Image')}}</x-label>
                        <x-input type="file" name="image_post"/>
                        <br>
                        <x-card.action> 
                            <x-button>{{__('Create post') }} </x-button>
                            <a href="/post" class="btn m-1 btn-primary">
                                {{__('Cancel') }}
                            </a>
                        </x-card.action>
                    </x-card.body>
            </x-card.wrapper>    
        </div>
    </div>
</x-container>
@endsection
@push('js')
@endpush
