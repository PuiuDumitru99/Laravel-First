@extends('layouts.base')
@section('page.title', $product->title) 

@section('content')

<x-container>
   <div class="row justify-content-center ">
    <div class="col col-6 ">
      <x-card.wrapper>
    <x-form.title>
            <a href="">
               {{$product->title}}
           </a>
           <a href="/post" class="btn btn-primary" title="Back to products" > <- </a>
    </x-form.title>           
               <img src="/img/furniture/{!!$product->img!!}" alt="">
            <x-form.item>
               {{$product->content}}
            </x-form.item>
          <x-form.item>
            Price:
            {{$product->price}}
          </x-form.item>
          
          <x-form.action action="{{route('busket.store',$product->id)}}" method="post">
            <x-butt-by>
             BUY
            </x-butt-by>
        </x-form.action>
            
           </x-card.wrapper>
           
        </div>
   </div>     
 </x-container>
@endsection
