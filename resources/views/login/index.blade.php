@extends('layouts.base')
@section('page.title') Login Page @endsection

@section('content')
    <x-container>
        <div class="row justify-content-center">
            <div class="col col-md-4 col-sm-6 ">
                <!-- Form for registration-->
            <x-card.wrapper>
                <x-card.body action="{{route('login.search')}}">
                    <x-card.title>{{ __('Login')}}</x-card.title>                       
                    <x-label for="">{{__('Email')}}</x-label>
                    <x-input name="email" required></x-input><br>
                    <x-label for="">{{__('Password')}}</x-label>
                    <x-input name="password" required></x-input><br>                
                    <x-card.action>
                        <x-button>{{_('Login')}}</x-button>
                        <x-a-link href="{{route('register.index')}}">
                            Register
                        </x-a-link>
                    </x-card.action>
                </x-card.body>
            </x-card.wrapper>
            </div>
        </div>        
    </x-container>
@endsection
