@extends('layouts.base')
@section('page.title') Registration @endsection

@section('content')
<x-container>
    <div class="row  justify-content-center">
        <div class="col col-md-4  col-sm-6">
            <x-card.wrapper> 
                <x-card.body action="{{route('register.store')}}" method="post">
                    <x-card.title>{{ __('Registration')}}</x-card.title>
                    <x-label for="name" >{{__('First Name')}}</x-label>
                    <x-input name="name" required /><br>
                    <x-label for="sname" >{{__('Second Name')}}</x-label>
                    <x-input name="sname" required /><br>
                    <x-form.item > 
                        <x-label  for="genderM">
                            {{__('Male')}}
                            <input required type="radio" id="genderM"/> 
                        </x-label>
                        <x-label for="genderF">
                            {{__('Female')}}
                            <input required type="radio" id="genderF" />
                    </x-label>
                    </x-form.item>
                    <x-label for="date"  >{{__('Birthday')}}</x-label>
                    <x-input type="date" name="date" required /><br>
                    <x-label for="email"  >{{__('Email')}}</x-label>
                    <x-input name="email" required /><br>
                    <x-label for="confirm_Email"  >{{__('Confirm Email')}}</x-label>
                    <x-input name="confirm_Email"  required/><br>
                    <x-label for="password"  >{{__('Password')}}</x-label>
                    <x-input name="password"  type="password" required /><br>
                    <x-label for="avatar"  >{{__('Avatar')}}</x-label>
                    <x-input name="avatar"  type="file"  /><br>
                    <x-checkbox name='accept' checked >{{__('I accept')}}</x-checkbox>
               <x-card.action>
                    <x-a-link href="{{route('user.login')}}">{{__('Back')}}</x-a-link>
                    <x-button >{{__('Register')}}</x-button>
                </x-card.action>  
            </x-card.body> 
            </x-card.wrapper>   
        </div>    
    </div> 
</x-container>

@push('js')
<script src='/js/checkbox.js'></script>
    
@endpush

@endsection

