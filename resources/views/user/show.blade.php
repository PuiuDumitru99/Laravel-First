@extends('layouts.base')
@section('page.title')
@endsection
@section('content')

<div class="row justify-content-center ">
   <div class="col col-6 ">
     <x-card.wrapper id="info">
         <x-form.title>
               <a href="" id="user_name">{{$user_name}}</a>
               <a href="/post" class="btn btn-primary" title="Back to products" > <- </a>
         </x-form.title> 
         <x-card.body>
            <img src="/img/user/{!!$user_avatar!!}.png" alt="">
            </x-card.body>
               <x-form.item id="user_email">{{$user_email}}</x-form.item>
      </x-card.wrapper>          
       </div>
  </div> 
@push('js')
<script src='/js/user.js'>

</script>
    
@endpush
@endsection