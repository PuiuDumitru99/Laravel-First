@extends('layouts.base')
@section('page.title') Busket Page @endsection

@section('content')

<x-container>
    <div class="row">
        <div class="col d-flex flex-wrap justify-content-center">     
            <div class="card p-2 border-3 mt-5 d-flex justify-content-center flex-row flex-wrap ">
                @if (empty($products))
                {{ __('Nothing post`s page')}}
                @else
                @foreach ($products as $product )  
                    <x-form.wrapper>  
                        <x-form.title>{{$product->title}}</x-form.title>
                        <x-form.body>
                        <img src="../img/furniture/{{$product->img}}" alt="">
                        </x-form.body>
                        <x-form.item>{{$product->price}}</x-form.item>
                        <x-form.action action="{{route('busket.show',$product->id)}}">
                            <x-button>X</x-button> 
                        </x-form.action>
                    </x-form.wrapper> 
                @endforeach
                <p class="card-title right">
                    <h3>
                        Total: {{$products_sum}}
                    </h3>
                </p>
                @endif
            </div> 

        </div>
    </div>
</x-container>
@endsection