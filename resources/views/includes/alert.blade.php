 @if ($alert=session()->pull('alert'))
 <div class="alert alert-success mt-1 small text-center" role="alert">
    {{$alert}}
 </div>
  @endif