<footer>
    <x-container>
        <div class="row justify-content-center mt-2 pt-2">
                 <!--Section for logo and link to social media-->
                    <div class="d-grid col">
                        <div class="logo">
                            <a href="/">Logo</a>
                        </div>
                        <p class="d-flex col-sm col-lg-6">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sedo
                            eiusmod tempor incdididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minzim veniam, quis nostrud exercitation ullamco 
                            laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                        <div class="d-flex social_media col ">
                            <a href=""><img src="/img/socialmedia/facebook.png" alt=""></a>
                            <a href=""><img src="/img/socialmedia/insta.png" alt=""></a>
                            <a href=""><img src="/img/socialmedia/linkedin.png" alt=""></a>
                            <a href=""><img src="/img/socialmedia/twitter.png" alt=""></a>
                            <a href=""><img src="/img/socialmedia/google.png" alt=""></a>
                            <a href=""><img src="/img/socialmedia/pinterest.png" alt=""></a>
                        </div>
                    </div>
                    <div class="d-flex col-4 align-items-center ">
                        <div class="col">
                            <h3>
                                Quick Links
                            </h3>
                            <ul>
                                <li>HOME</li>
                                <li>ABOUT US</li>
                                <li>BLOG</li>
                                <li>PRODUCTS</li>
                            </ul>
                        </div>
                        <div class="col">
                            <h3>
                                Support
                            </h3>
                            <ul>
                                <li>HELP</li>
                                <li>CONTACT</li>
                                <li>POLICY</li>
                                <li>SERVICE</li>
                            </ul>

                        </div>
                    </div>
                </div>
    </x-container>

        <div class="d-flex  justify-content-center copyright">
        © {{config('app.name')}} {{$date}}</div>
</footer>