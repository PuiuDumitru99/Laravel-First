<header class="d-grid border-none mt-2 mb-1">
  <!--First menu-->
    <div class="row">
        <nav class="menu1">
            <!--Box for number phone-->
            <div class="d-flex col align-items-center ">
                <label class="m-2 d-none d-xl-block" for="">+91998 654 6583</label>
                <label class="m-2 d-none d-xl-block" for="">Welcome you Benco store!</label>
            </div>
            <div class="d-flex d-none d-sm-flex d-xl-flex">
                <button class="btn-trans">Currency:</button>
                <a href="/login">
                    <button class="btn-brown">My Account</button>
                </a>
            </div>
        </nav>
    </div>
 <!--Second menu-->
    <nav class="row mt-1 ">
        <!-- section for menu and logo-->
            <div class="col d-flex align-items-center">
            <!-- DIV for logo-->
                <div class="col-3 logo">
                    <a href="/">Logo</a>
                </div>
            <!-- DIV for list menu-->
                <div class="col d-none d-xl-block  menu-list ">
                    <a href="/">Home</a>
                    <a href="">Shop</a>
                    <a href="">Blog</a>
                    <a href="/about">About Us</a>
                    <a href="/post">Product</a>
                    <a href="">Pages</a>
                    <a href="contact">Contact Us</a>
                </div>
            </div>
        <!-- section for shopping  -->
            <div class="col-2 col-sm-2 col-lg-2 d-flex align-items-center justify-content-around ">
                <div class="search"></div>
                <a href="\busket" class="shopping-cart">--
                    
                </a>
                <a href="/user/show/" class="user">
                    </a> 
            </div>        
    </nav>
    @push('js')
        <script src="/js/count.js"></script>
    @endpush
    
</header>