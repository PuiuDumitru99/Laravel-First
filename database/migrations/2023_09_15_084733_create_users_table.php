<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) 
        {
            $table->id()->autoIncrement()->nullable();
            $table->timestamps();
            $table->string('name');
            $table->string('sname');
            $table->string('gender');
            $table->date('Bdate');
            $table->string('email')->unique();
            $table->string('avatar')->nullable();
            $table->boolean('active')->default(true);
            $table->string('password');
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
