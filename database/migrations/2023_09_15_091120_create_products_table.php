<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {

            $table->id();
           //$table->bigInteger('user_id')->unsigned()->nullable();
           // $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
           
           $table->foreignId('user_id')->constrained()->cascadeOnDelete();

            $table->string('img');
            $table->string('title');
            $table->string('content');
            $table->integer('price');
            
            $table->boolean('active')->default(true);

            $table->timestamps();
            $table->timestamp('published_at')->nullable();
            

            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
