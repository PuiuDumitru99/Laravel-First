<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Contracts\View\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
        view()->share('date', date('Y'));

        Paginator::useBootstrapFive();
    }
    
}
