<?php

use Illuminate\Support\Facades\Route;

if(! function_exists('active_link'))
{
    function active_link(string $name, string $active='active'): string
    {
        return Route::is($name)? $active :'';
    }
}
if(! function_exists('alert'))
{
    function alert(string $value)
    {
       session(['alert' => $value]);
    }

}
if(! function_exists('wrong'))
{
    function wrong(string $value)
    {
       session(['wrong' => $value]);
    }

}
if(! function_exists('validate'))
{
    function validate(array $attibutes,array $rules ):array
    {
      return validator($attibutes, $rules)->validate();
    }

}