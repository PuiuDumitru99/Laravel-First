<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Busket extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'user_email',
        'title','img',
        'price',
        'product_id',
        ] ;
        protected $casts = 
        [
         'add_at'=>'datetime',
        ] ;
}
