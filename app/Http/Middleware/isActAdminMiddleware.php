<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class isActAdminMiddleware
{
    /**
     * Handle an incoming request.
     
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($this->requestAdmin($request))
        {
            return $next($request);
        }
        else
        {
            
           abort(403);
        }
        
    }
    protected function requestAdmin($request)
    {
        //return false;
        return true;
               
    }
}
