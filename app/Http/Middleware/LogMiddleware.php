<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if ($this->isActive($request))
        {
            return $next($request);
        }
        else
        {

           info($request->all());

            abort(403);

        }
        
    }


    protected function isActive($request)
    {

        return true;
    }
}
