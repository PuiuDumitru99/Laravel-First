<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view ('login.index');
    }

    public function store(Request $request)
    {
        
    }
    protected function search(Request $request)
    {   
        $user_mail=$request->email;  
        $user_password=$request->password; 

        $user=User::query()->get(['email','password'])->where('email',$user_mail);
       
        if($user->value('password')===$user_password)
        {
            alert(__('You login with success')); 
            $user_id=$user->value('email');
             return redirect('/user/show/'.$user_id);
         }
        else
        {
            wrong(__('Something went wrong'));
            return redirect('/login');
        }

    }
}
 