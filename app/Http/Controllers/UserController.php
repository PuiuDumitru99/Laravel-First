<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){

        return view ('user.index',);
    }
    public function show($user_id)
    {

        $user=User::query()->where('email',$user_id)->get(['name','avatar','email']); 
        $user_name=$user->value('name'); 
        $user_avatar=$user->value('avatar'); 
        $user_email=$user->value('email');
        return view ('user.show',['user_name'=>$user_name,'user_avatar'=>$user_avatar,'user_email'=>$user_email]);
        
    }
}
