<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User as ModelsUser;
use App\Models\Product as ModelsProduct;
use App\Models\Busket as BusketModel;



class BusketController extends Controller
{
    public function index()
    {
        $products=BusketModel::query()->get(['id','img','title','price']);
        $products_sum=BusketModel::query()->sum("price");
        return view ('busket.index',compact('products'),['products_sum'=> $products_sum]);
    }

    public function store(Request $request,$product)
    {
        $busket=ModelsProduct::query()->findOrFail($product,['id','title','img','price']);

        $busket=BusketModel::query()->firstOrCreate([
            'user_id'=>ModelsUser::query()->value('id'),  
            'user_email'=>ModelsUser::query()->value('email'),
            'id'=> $busket->id,
            'title'=>$busket->title,  
            'img'=>$busket->img, 
            'price'=>$busket->price,
            'product_id'=>fake()->numerify(),
        ]);
        $busket->save();
        alert(__('You add products with success'));
       return back();
    }
    public function show($busket)
    {      
        $busket=BusketModel::query()->findOrFail($busket,['id']);   
        $delete_id=$busket->id;    
        //dd($busket->id);
        $busket=BusketModel::query()->where('id',$delete_id)->delete();
        //dd($buskets);
        return back();

    }
    
    
}
