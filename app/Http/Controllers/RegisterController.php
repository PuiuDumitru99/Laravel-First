<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\View\Components\input;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    
    public function index()
    {
         return view('login.register');
    }
   
    public function store(Request $request)
    {
        function gen($date){
            if($date==true){
            return 'M'; }
            else{return 'F';}
        };
        
        if($user=user::query()->get(['email'])->where('email',$request->email)->first()==true)
        {
            wrong(__('That user are exist'));
            return view('login.register');
        }

        else {
        $user=User::query()->create([
            'name'=> $request->input('name'),
            'sname'=> $request->input('sname'),
            'Bdate'=> $request->input('date'),
            'email'=> $request->email,
            'gen'=> gen($request->input('genderM')),
            'avatar'=>$request->input('avatar'),
            'confirm_email'=>$request->confirm_email,
            'remember_token'=>'TOKEN',
            'password'=> $request->password,
        ]);
        //dd($user);
        $user->save(); 
        alert(__('You registred with success'));  
        return redirect('/user/show/'.$request->email);
        }
    }   
}
