<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as ModelsUser;
use App\Models\Product as ModelsProduct;


use Illuminate\Support\Carbon;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {    
        // $database= ModelsPost::all(['id','title','content']);
        // $posts=$database->toArray();  
        $con=mysqli_connect('127.0.0.1','root','');
        mysqli_select_db($con,'laravel') or ('No database found');
    
        if (mysqli_select_db($con,'laravel')) 
        {
            $products= ModelsProduct::query()->paginate(10);   
           // dd($products);     
            return  view('post.index',compact('products'));
           
        } else {
            
            abort(404);
            return  view('post.index');
        }
    }
    /**P
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return  view('post.create') ;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {        
        // $product=modelsProduct::query()->firstOrCreate([
            
            // 'user_id'=>ModelsUser::query()->value('id') ?? null,
            // 'title'=> $request->title_post ?? null,
            // ],[
            // 'img'=>$request->image_post ?? 'NoImage.png',
            // 'price'=>fake()->numerify(),
            // 'active'=>true,
            // 'content'=>$request->data_post,
            // 'published_at'=>new Carbon($request->published_at)?? null,
            // ]);            
            // $product->save();

         for($i=0; $i<15; $i++ )
         {
            $product=modelsProduct::query()->firstOrCreate([
                'user_id'=>ModelsUser::query()->value('id'),
                'title'=>fake()->sentence(),
                ],[
                'img'=>$request->image_post ?? 'NoImage.png',
            'price'=>fake()->numerify(),
            'active'=>true,
            'content'=>fake()->text(),
            'published_at'=>new Carbon($request->published_at)?? null,
            ]);
            
            $product->save();
            }
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     */
    public function show($product)
    {
        //  $product=modelsProduct::query()->get(['id','img','title','price']);
        
         $product=modelsProduct::query()->findOrFail($product,['id','img','title','content','price']); 
         //dd($product);
         //$product=modelsProduct::query()->where('id',$product->id);

        return view('post.show',compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        
    }
    public function search(Request $request)
    {        
         $search=$request->input('search');    

         if($product=modelsProduct::query()->get(['title'])->where('title',$search)->first()==true)
         {
            $products=modelsProduct::query()->get(['id','title','content','price'])
            ->where('title',$search);
            $product_count=$products->count();
            return view('post.index',compact('products'), ['product_count'=>$product_count]);
        }
         else 
         {           
            return view('post.index') ; 
                        
        }    

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
