<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product as ModelsProduct;
use App\Models\busket as BusketModel;
use App\Models\user as ModelsUser;

class HomeController extends Controller
{
    public function index(Request $request) 
    {
        $products=ModelsProduct::query()->get(['id','img','title','price']);
        //dd($posts);
        return view('home.index',compact('products'));    
    }
    public function view(Request $request)
    {
        
       
    }
    public function show(Request $request ){
    }

}
